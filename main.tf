resource "null_resource" "wait_for_resources" {
  triggers = {
    depends_on = join("", var.wait_for_resources)
  }
}

resource "random_password" "master_adminPassword" {
  depends_on = ["null_resource.wait_for_resources"]
  length = 32
  special = true
  override_special = "_%@"
}

locals {
  cluster_domain = "${var.CLUSTER_NAME}.${var.techturbidDOMAIN_NAME}"
}

resource "helm_release" "jenkins" {
  depends_on = [random_password.master_adminPassword]
  chart = "stable/jenkins"
  name = "jenkins"
  namespace = var.NAMESPACE

  set {
    name = "fullnameOverride"
    value = "jenkins"
  }

  set {
    name = "master.serviceType"
    value = "ClusterIP"
  }

  set {
    name = "master.adminPassword"
    value = random_password.master_adminPassword.result
  }
}

resource "helm_release" "jenkins-vs" {
  depends_on = [helm_release.jenkins]
  name       = "jenkins-vs"
  namespace = helm_release.jenkins.namespace
  chart      = "${path.module}/istio/virtualservice"

  set {
    name = "virtualService.host"
    value = "${helm_release.jenkins.name}.${local.cluster_domain}"
  }
  set {
    name = "istio.gateway"
    value = var.istio_gateway
  }
  set {
    name = "virtualService.name"
    value = helm_release.jenkins.name
  }
  set {
    name = "virtualService.service"
    value = helm_release.jenkins.name
  }
  set {
    name = "virtualService.namespace"
    value = helm_release.jenkins.namespace
  }
}

output "jenkins_url" {
  value = "${helm_release.jenkins.name}.${local.cluster_domain}"
}

output "jenkins_password" {
  value = random_password.master_adminPassword.result
}
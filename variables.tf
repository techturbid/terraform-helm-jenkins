variable "wait_for_resources" {}
variable "NAMESPACE" {
  default = "cicd"
}
variable "techturbidDOMAIN_NAME" {
  default = "techtur.bid"
}
variable "CLUSTER_NAME" {}
variable "istio_gateway" {
default = "public-gateway.istio-system.svc.cluster.local"
}
